package com.bayudamar.pngsequencebw


import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_onboarding1.view.*

/**
 * A simple [Fragment] subclass.
 */
class Onboarding4Fragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_onboarding4, container, false)
        view.splashSpinner.post {
            val spinnerAnim = view.splashSpinner.background as AnimationDrawable
            if (!spinnerAnim.isRunning) {
                spinnerAnim.start()
            }
        }
        return view
    }


}
