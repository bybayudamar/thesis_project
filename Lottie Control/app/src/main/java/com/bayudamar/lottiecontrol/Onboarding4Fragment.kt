package com.bayudamar.lottiecontrol


import android.animation.ValueAnimator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_onboarding4.view.*

/**
 * A simple [Fragment] subclass.
 */
class Onboarding4Fragment : Fragment() {
    private var mView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_onboarding4, container, false)
        return mView
    }

    fun playAnimationFirstHalf() {
        val animator = ValueAnimator.ofFloat(0f, 0.5f).setDuration(1500)
        animator.addUpdateListener { valueAnimator ->
            mView?.lottie?.progress = valueAnimator.animatedValue as Float
        }
        animator.start()
    }

    fun playAnimationSecondHalf() {
        val animator = ValueAnimator.ofFloat(0.5f, 1f).setDuration(1500)
        animator.addUpdateListener { valueAnimator ->
            mView?.lottie?.progress = valueAnimator.animatedValue as Float
        }
        animator.start()
    }

}
