package com.bayudamar.lottiecontrol

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val ob1 = Onboarding1Fragment()
    private val ob2 = Onboarding2Fragment()
    private val ob3 = Onboarding3Fragment()
    private val ob4 = Onboarding4Fragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewpager.adapter = object : FragmentStatePagerAdapter(supportFragmentManager) {
            override fun getItem(position: Int): Fragment {
                return when (position) {
                    0 -> ob1
                    1 -> ob2
                    2 -> ob3
                    3 -> ob4
                    else -> Fragment()
                }
            }

            override fun getCount(): Int {
                return 4
            }

        }

        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        ob1.playAnimationFirstHalf()
                    }
                    1 -> {
                        ob2.playAnimationFirstHalf()
                    }
                    2 -> {
                        ob3.playAnimationFirstHalf()
                    }
                    3 -> {
                        ob4.playAnimationFirstHalf()
                    }
                }
            }
        })
    }
}
